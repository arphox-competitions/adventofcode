﻿namespace AoC21.Day07;

internal sealed class Day07Solver : DaySolver
{
    internal override string Part1(AoCInput input2)
    {
        int[] input = input2.IntegerCsv();

        int min = input.Min();
        int max = input.Max();

        int[] ordered = input.OrderBy(x => x).ToArray();

        Dictionary<int, long> sums = new();
        for (int i = min; i <= max; i++)
        {
            int currentValue = i;
            long totalFuel = 0;
            for (int j = 0; j < input.Length; j++)
            {
                int diff = Math.Abs(currentValue - input[j]);
                totalFuel += diff;
            }
            sums[currentValue] = totalFuel;
        }

        return sums.Min(x => x.Value).ToString();
    }

    internal override string Part2(AoCInput input2)
    {
        int[] input = input2.IntegerCsv();

        int min = input.Min();
        int max = input.Max();

        int[] ordered = input.OrderBy(x => x).ToArray();

        Dictionary<int, long> sums = new();
        for (int i = min; i <= max; i++)
        {
            int currentValue = i;
            long totalFuel = 0;
            for (int j = 0; j < input.Length; j++)
            {
                int diff = Math.Abs(currentValue - input[j]);
                diff = (diff * (diff + 1)) / 2;
                totalFuel += diff;
            }
            sums[currentValue] = totalFuel;
        }

        return sums.Min(x => x.Value).ToString();
    }
}
