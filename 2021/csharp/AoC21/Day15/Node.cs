﻿namespace AoC21.Day15;

internal sealed class Node
{
    internal int Weight { get; }
    internal HashSet<Node> Neighbors { get; } = new();
    internal bool IsProcessed { get; set; }

    internal Node(int weight) => Weight = weight;

    public override string ToString() => Weight.ToString()!;
}
