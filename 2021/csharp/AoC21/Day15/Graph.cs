﻿namespace AoC21.Day15;

internal sealed class Graph
{
    internal Node StartNode { get; }
    internal Node EndNode { get; }
    internal IReadOnlyCollection<Node> Nodes { get; }

    internal Graph(Node startNode, Node endNode, IReadOnlyCollection<Node> allNodes)
    {
        StartNode = startNode ?? throw new ArgumentNullException(nameof(startNode));
        EndNode = endNode ?? throw new ArgumentNullException(nameof(endNode));
        Nodes = allNodes ?? throw new ArgumentNullException(nameof(allNodes));
    }
}
