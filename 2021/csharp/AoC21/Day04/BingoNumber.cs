﻿namespace AoC21.Day04;

internal sealed class BingoNumber
{
    internal int Value { get; }
    internal bool IsMarked { get; private set; } = false;
    internal bool IsNotMarked => !IsMarked;

    internal BingoNumber(int value) => Value = value;

    internal void MarkIfMatches(int number)
    {
        if (Value == number)
            IsMarked = true;
    }

    public override string ToString() => $"{Value} {(IsMarked ? "✔" : "❌")}";
}
