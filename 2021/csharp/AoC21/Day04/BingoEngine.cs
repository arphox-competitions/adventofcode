﻿namespace AoC21.Day04;

internal sealed class BingoEngine
{
    private readonly IReadOnlyList<int> _numbersToDraw;
    private readonly List<BingoBoard> _boards;

    internal BingoEngine(string[] fileRows)
    {
        _numbersToDraw = fileRows[0].Split(',').Select(int.Parse).ToArray();
        _boards = ParseBoards(fileRows);
    }

    internal BingoEngineResult RunToFirstWinner()
    {
        foreach (int drawnNumber in _numbersToDraw)
        {
            MarkMatchingNumbers(drawnNumber);
            BingoBoard[] winningBoards = GetWinners();
            if (winningBoards.Length == 0)
                continue;

            (BingoBoard winner, int winnerScore) = FindWinner(winningBoards, drawnNumber);
            return BingoEngineResult.CreateWinning(winner, winnerScore);
        }

        return BingoEngineResult.CreateNotWinning();
    }

    internal BingoEngineResult RunToLastWinner()
    {
        foreach (int drawnNumber in _numbersToDraw)
        {
            MarkMatchingNumbers(drawnNumber);
            BingoBoard[] winningBoards = GetWinners();
            if (_boards.Count > 1)
            {
                winningBoards.ForEach(x => _boards.Remove(x));
                continue;
            }

            if (_boards.Count != 1)
                throw new Exception("At the end, exactly one board must be a winner, " +
                    "but more than one was winning at the last draw.");

            if (winningBoards.Length == 0)
                continue;

            BingoBoard winner = winningBoards.Single();
            int winnerScore = winner.CalculateScore(drawnNumber);
            return BingoEngineResult.CreateWinning(winner, winnerScore);
        }

        return BingoEngineResult.CreateNotWinning();
    }

    private void MarkMatchingNumbers(int drawnNumber)
        => _boards.ForEach(b => b.MarkMatchingNumbers(drawnNumber));

    private BingoBoard[] GetWinners()
        => _boards.Where(b => b.IsWinning()).ToArray();

    private static (BingoBoard winner, int winnerScore) FindWinner(
        IEnumerable<BingoBoard> winningBoards,
        int drawnNumber)
    {
        int winnerScore = int.MinValue;
        BingoBoard winner = null!;
        foreach (BingoBoard board in winningBoards)
        {
            int score = board.CalculateScore(drawnNumber);
            if (score > winnerScore)
            {
                winnerScore = score;
                winner = board;
            }
        }

        return (winner, winnerScore);
    }

    private static List<BingoBoard> ParseBoards(string[] fileRows)
    {
        List<BingoBoard> boards = new List<BingoBoard>();
        int i = 2;
        while (i < fileRows.Length)
        {
            if (fileRows[i] == "")
            {
                i++;
                continue;
            }

            if (i + 5 >= fileRows.Length)
                break;

            boards.Add(new BingoBoard(fileRows, i, i + 5));
            i += 5;
        }

        return boards;
    }
}
