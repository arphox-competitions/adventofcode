﻿using System.Numerics;
using System.Text;

namespace AoC21.Day17;

internal sealed class Day17Solver : DaySolver
{
    internal override string Part1(AoCInput inputRaw)
    {
        Day17Input input = Day17Input.Parse(inputRaw.Value);

        // someone wrote on reddit that the following formula works too:
        //      abs(ymin) * (abs(ymin) - 1) / 2
        return (Math.Abs(input.Yfrom) * (Math.Abs(input.Yfrom) - 1) / 2).ToString();

        // Below is my original brute force solution:
        int maxY = 0;
        for (int x = 0; x < 1000; x++)
        {
            Console.WriteLine($"X={x}, max so far: {maxY}");
            for (int y = -1000; y < 1000; y++)
            {
                int? result = SimulatePart1(input, x, y);
                if (result != null && result > maxY)
                    maxY = result.Value;
            }
        }

        return maxY.ToString();
    }

    internal override string Part2(AoCInput inputRaw)
    {
        Day17Input input = Day17Input.Parse(inputRaw.Value);
        int successCount = 0;
        for (int x = 0; x < 1000; x++)
        {
            Console.WriteLine($"X={x}, count so far: {successCount}");
            for (int y = -1000; y < 1000; y++)
            {
                int? result = SimulatePart1(input, x, y);
                if (result != null)
                    successCount++;
            }
        }

        return successCount.ToString();
    }

    private static int? SimulatePart1(Day17Input input, int initialVelocityX, int initialVelocityY)
    {
        int probeX = 0;
        int probeY = 0;
        int maxY = int.MinValue;
        int stepCounter = 0;
        while (true)
        {
            // Break if inside target area
            if (probeX >= input.Xfrom && probeX <= input.Xto &&
                probeY >= input.Yfrom && probeY <= input.Yto)
            {
                return maxY;
            }
            // or if overshoot
            if (probeX > input.Xto || probeY < input.Yfrom)
            {
                return null;
            }

            // Do step
            probeX += initialVelocityX;
            probeY += initialVelocityY;

            if (initialVelocityX > 0)
                initialVelocityX--;
            else if (initialVelocityX < 0)
                initialVelocityX++;

            initialVelocityY--;

            if (probeY > maxY)
                maxY = probeY;

            stepCounter++;
        }
    }

    private static void VisualizeStepResult(Vector2 probeLoc, Day17Input input)
    {
        Console.Clear();
        StringBuilder sb = new();
        int maxY = Math.Max((int)probeLoc.Y, 0);
        int minY = Math.Min((int)probeLoc.Y, input.Yfrom);
        int maxX = Math.Max((int)probeLoc.X, input.Xto);
        for (int y = maxY; y >= minY; y--)
        {
            for (int x = 0; x < maxX; x++)
            {
                if (x == 0 && y == 0)
                    sb.Append('S');
                else if (x == probeLoc.X && y == probeLoc.Y)
                    sb.Append('#');
                else if (x >= input.Xfrom && x <= input.Xto && y >= input.Yfrom && y <= input.Yto)
                    sb.Append('T');
                else
                    sb.Append('.');
            }
            sb.AppendLine();
        }
        Console.WriteLine(sb.ToString());
        Console.ReadLine();
    }
}
