﻿using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace AoC21.Day18;

internal sealed class Day18Solver : DaySolver
{
    internal override string Part1(AoCInput inputRaw)
    {
        SnailfishNumber[] input = ParseInput(inputRaw);
        SnailfishNumber added = input.Aggregate((a, b) => CompositeSnailfishNumber.Add(a, b));
        return added.CalculateMagnitude().ToString();
    }

    internal override string Part2(AoCInput inputRaw)
    {
        SnailfishNumber[] input = ParseInput(inputRaw);
        long largestMagnitude = long.MinValue;
        SnailfishNumber? largest = null;
        for (int i = 0; i < input.Length; i++)
        {
            for (int j = i + 1; j < input.Length; j++)
            {
                // i + j
                SnailfishNumber current = CompositeSnailfishNumber.Add(input[i], input[j]);
                long magnitude = current.CalculateMagnitude();
                if (magnitude > largestMagnitude)
                {
                    largestMagnitude = magnitude;
                    largest = current;
                }

                // j + i
                current = CompositeSnailfishNumber.Add(input[j], input[i]);
                magnitude = current.CalculateMagnitude();
                if (magnitude > largestMagnitude)
                {
                    largestMagnitude = magnitude;
                    largest = current;
                }
            }
        }

        return largestMagnitude.ToString();
    }

    private static SnailfishNumber[] ParseInput(AoCInput input)
    {
        return input
            .SplitByNewLines()
            .Select(ParseLine)
            .ToArray();
    }

    internal static SnailfishNumber ParseLine(string line)
    {
        int index = 0;
        SnailfishNumber result = Parse(null);
        if (result.ToString() != line)
            throw new Exception("parsing is bugged");
        return result;

        // -----------------------
        SnailfishNumber Parse(CompositeSnailfishNumber? parent)
        {
            char ch = line[index++];
            if (ch == '[')
            {
                var self = new CompositeSnailfishNumber(parent);
                self.X = Parse(self);
                if (line[index++] != ',') throw new Exception();
                self.Y = Parse(self);
                if (line[index++] != ']') throw new Exception();
                return self;
            }
            else if (char.IsDigit(ch))
            {
                int value;
                // If next char is digit as well (multi-digit number)
                if (index < line.Length && char.IsDigit(line[index]))
                {
                    StringBuilder sb = new();
                    sb.Append(ch);
                    while (index < line.Length && char.IsDigit(line[index]))
                    {
                        sb.Append(line[index]);
                        index++;
                    }
                    value = int.Parse(sb.ToString());
                }
                else
                {
                    value = (int)char.GetNumericValue(ch);
                }
                return new LiteralSnailfishNumber(value, parent);
            }
            else
            {
                throw new Exception();
            }
        }
    }

    internal static SnailfishNumber Reduce(SnailfishNumber number)
    {
        bool modified;
        SnailfishNumber result = number;
        do
        {
            modified = false;
            result = TryExplode(result);
            if (!modified)
                result = TrySplit(result);
        }
        while (modified);
        return result;

        // ----------------------------
        SnailfishNumber TryExplode(SnailfishNumber number)
        {
            if (number is CompositeSnailfishNumber composite)
            {
                int depth = composite.CalculateDepth();
                if (depth == 4)
                {
                    modified = true;
                    Explode(number.Parent!, composite);
                }
                if (depth > 4) throw new Exception();

                TryExplode(composite.X);
                TryExplode(composite.Y);
            }
            return number;
        }
        SnailfishNumber TrySplit(SnailfishNumber number)
        {
            if (number is CompositeSnailfishNumber composite)
            {
                composite.X = TrySplit(composite.X);
                if (modified)
                    return composite;

                composite.Y = TrySplit(composite.Y);
                return composite;
            }
            else if (number is LiteralSnailfishNumber literal)
            {
                if (literal.Value < 10)
                    return number;

                modified = true;
                return Split(literal);
            }
            return number;
        }
    }

    internal static CompositeSnailfishNumber Split(LiteralSnailfishNumber literal)
    {
        int value = literal.Value;
        int left = value / 2;
        int right = value - left;

        var composite = new CompositeSnailfishNumber(literal.Parent);
        composite.X = new LiteralSnailfishNumber(left, composite);
        composite.Y = new LiteralSnailfishNumber(right, composite);
        return composite;
    }

    internal static void Explode(CompositeSnailfishNumber number, CompositeSnailfishNumber childToExplode)
    {
        if (childToExplode == number.X)
        {
            number.X = ExplodeReplace((CompositeSnailfishNumber)number.X);
        }
        else if (childToExplode == number.Y)
        {
            number.Y = ExplodeReplace((CompositeSnailfishNumber)number.Y);
        }
        else throw new Exception();
    }

    private static LiteralSnailfishNumber ExplodeReplace(CompositeSnailfishNumber target)
    {
        Climb((LiteralSnailfishNumber)target.X, "left");
        Climb((LiteralSnailfishNumber)target.Y, "right");
        return new LiteralSnailfishNumber(0, target.Parent);

        // ------------------------------------------------------------------------
        static void Climb(LiteralSnailfishNumber sourceLiteral, string direction)
        {
            List<SnailfishNumber?> visitedNodes = new() { sourceLiteral.Parent };
            CompositeSnailfishNumber? parent = sourceLiteral.Parent!.Parent;
            while (parent != null)
            {
                SnailfishNumber child = direction == "left" ? parent.X : parent.Y;
                if (!visitedNodes.Contains(child))
                {
                    string otherDirection = direction == "left" ? "right" : "left";
                    LiteralSnailfishNumber nodeToModify = child.GetLeafNode(otherDirection);
                    nodeToModify.Value += sourceLiteral.Value;
                    return;
                }
                else
                {
                    visitedNodes.Add(parent!);
                    parent = parent.Parent;
                }
            }
        }
    }
}

internal abstract class SnailfishNumber
{
    internal CompositeSnailfishNumber? Parent { get; set; }
    protected SnailfishNumber(CompositeSnailfishNumber? parent) => Parent = parent;
    internal abstract LiteralSnailfishNumber GetLeafNode(string direction);
    internal abstract long CalculateMagnitude();
    internal SnailfishNumber DeepClone() => Day18Solver.ParseLine(ToString()!);
}
internal class CompositeSnailfishNumber : SnailfishNumber
{
    internal SnailfishNumber X { get; set; }
    internal SnailfishNumber Y { get; set; }

#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    internal CompositeSnailfishNumber(CompositeSnailfishNumber? parent)
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
        : base(parent)
    {
    }

    internal CompositeSnailfishNumber(SnailfishNumber x, SnailfishNumber y, CompositeSnailfishNumber? parent)
        : this(parent)
    {
        X = x ?? throw new ArgumentNullException(nameof(x));
        Y = y ?? throw new ArgumentNullException(nameof(y));
    }

    internal int CalculateDepth()
    {
        CompositeSnailfishNumber? parent = Parent;
        int i = 0;
        while (parent != null)
        {
            i++;
            parent = parent.Parent;
        }
        return i;
    }

    public override string ToString() => $"[{X},{Y}]";

    internal static SnailfishNumber Add(SnailfishNumber left, SnailfishNumber right)
    {
        SnailfishNumber leftCLone = left.DeepClone();
        SnailfishNumber rightClone = right.DeepClone();

        CompositeSnailfishNumber sum = new(leftCLone, rightClone, null);
        leftCLone.Parent = sum;
        rightClone.Parent = sum;
        return Day18Solver.Reduce(sum);
    }

    internal override LiteralSnailfishNumber GetLeafNode(string direction)
    {
        return direction switch
        {
            "left" => X.GetLeafNode(direction),
            "right" => Y.GetLeafNode(direction),
            _ => throw new Exception(),
        };
    }

    internal override long CalculateMagnitude()
    {
        long left = 3 * X.CalculateMagnitude();
        long right = 2 * Y.CalculateMagnitude();
        return left + right;
    }
}

internal sealed class LiteralSnailfishNumber : SnailfishNumber
{
    internal int Value { get; set; }
    internal LiteralSnailfishNumber(int value, CompositeSnailfishNumber? parent)
        : base(parent)
    {
        Value = value;
    }

    public override string ToString() => Value.ToString();
    internal override LiteralSnailfishNumber GetLeafNode(string direction) => this;
    internal override long CalculateMagnitude() => Value;
}
