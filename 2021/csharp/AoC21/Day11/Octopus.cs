﻿namespace AoC21.Day11;

internal sealed class Octopus
{
    public Octopus(int energyLevel, int rowIndex, int colIndex)
    {
        EnergyLevel = energyLevel;
        RowIndex = rowIndex;
        ColIndex = colIndex;
    }

    public int EnergyLevel { get; set; }
    public int RowIndex { get; }
    public int ColIndex { get; }
    public bool Flashed { get; set; }
    public void ResetIfFlashed()
    {
        if (Flashed)
        {
            EnergyLevel = 0;
            Flashed = false;
        }
    }
}
