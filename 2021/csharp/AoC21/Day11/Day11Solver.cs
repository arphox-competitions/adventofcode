﻿namespace AoC21.Day11;

internal sealed class Day11Solver : DaySolver
{
    internal override string Part1(AoCInput input)
    {
        Octopus[,] map = ParseMap(input);
        int flashCounter = 0;
        for (int i = 0; i < 100; i++)
            flashCounter += DoStep(map);

        return flashCounter.ToString();
    }

    private static Octopus[,] ParseMap(AoCInput input)
    {
        return input
            .Integer2DArrayWithoutSeparator()
            .Select(x => new Octopus(x.Value, x.Index0, x.Index1));
    }

    private static int DoStep(Octopus[,] map)
    {
        map.ForEach(o => o.EnergyLevel++);
        while (true)
        {
            bool repeat = false;
            map.ForEach(o =>
            {
                if (o.EnergyLevel > 9 && !o.Flashed)
                {
                    o.Flashed = true;
                    repeat = true;
                    map.GetNeighborsCloserThanTwo(o.RowIndex, o.ColIndex)
                       .ForEach(o => o.EnergyLevel++);
                }
            });
            if (!repeat) break;
        }

        int flashCount = map.Cast<Octopus>().Count(o => o.Flashed);
        map.ForEach(o => o.ResetIfFlashed());
        return flashCount;
    }

    internal override string Part2(AoCInput input)
    {
        Octopus[,] map = ParseMap(input);
        int stepCounter = 0;
        while (true)
        {
            stepCounter++;
            int flashCount = DoStep(map);
            if (flashCount == 100)
                return stepCounter.ToString();

            if (stepCounter % 1000 == 0)
                Console.WriteLine("step count is at " + stepCounter);
        }
    }

    private static void PrintMap(Octopus[,] map)
    {
        for (int row = 0; row < map.GetLength(0); row++)
        {
            for (int col = 0; col < map.GetLength(1); col++)
            {
                Console.Write(map[row, col]);
            }
            Console.WriteLine();
        }
        Console.WriteLine();
    }
}
