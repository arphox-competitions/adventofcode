﻿using System.Numerics;

namespace AoC21.Day19;

internal sealed class Day19Solver : DaySolver
{
    internal override string Part1(AoCInput inputRaw)
    {
        ScannerData[] input = ParseInput(inputRaw);
        List<((ScannerData, ScannerData), int)> overlaps = new();

        for (int i = 0; i < input.Length; i++)
        {
            ScannerData scanner1 = input[i];
            for (int j = i + 1; j < input.Length; j++)
            {
                ScannerData scanner2 = input[j];
                int overlapCount = OverlapCount(scanner1.BeaconDistances, scanner2.BeaconDistances);
                overlaps.Add(((scanner1, scanner2), overlapCount));
            }
        }

        List<((ScannerData sc1, ScannerData sc2), int overlapCount)> overlapRanking = overlaps.OrderByDescending(o => o.Item2).ToList();

        Console.ReadLine();
        return "";
    }

    private static ScannerData[] ParseInput(AoCInput inputRaw)
    {
        return inputRaw.Value
            .Split(Environment.NewLine + Environment.NewLine, StringSplitOptions.RemoveEmptyEntries)
            .Select(ScannerData.Parse)
            .ToArray();
    }

    private static long[] Part1_CalculateDistancesFromScannerToBeacons(ScannerData scannerData)
    {
        return scannerData.Beacons
            .Select(x => (long)x.LengthSquared())
            .ToArray();
    }

    internal override string Part2(AoCInput input)
    {
        return "";
    }

    private static int OverlapCount<T>(T[] a, T[] b)
    {
        List<T> copy = b.ToList();
        return a.Count(item =>
        {
            int index = copy.BinarySearch(item);
            if (index < 0)
                return false;

            copy.RemoveAt(index);
            return true;
        });
    }
}

internal sealed class ScannerData
{
    internal string Id { get; }
    internal IReadOnlyList<Vector3> Beacons { get; }
    internal long[] BeaconDistances { get; }

    internal ScannerData(string id, IReadOnlyList<Vector3> beacons)
    {
        if (string.IsNullOrWhiteSpace(id))
            throw new ArgumentException($"'{nameof(id)}' cannot be null or whitespace.", nameof(id));
        Id = id;

        Beacons = beacons ?? throw new ArgumentNullException(nameof(beacons));
        BeaconDistances = CalculateDistancesBetweenBeacons(this);
    }

    public override string ToString() => $"scanner #{Id} with {Beacons.Count} beacons";

    internal static ScannerData Parse(string input)
    {
        string[] lines = input.Split(Environment.NewLine);

        string id = lines[0]
            .Replace("--- scanner ", "")
            .Replace(" ---", "");

        Vector3[] beacons = lines
            .Skip(1)
            .Select(ParseVector)
            .ToArray();

        return new ScannerData(id, beacons);
    }

    private static Vector3 ParseVector(string line)
    {
        string[] parts = line.Split(',');
        if (parts.Length > 3) throw new Exception();

        int x = int.Parse(parts[0]);
        int y = int.Parse(parts[1]);
        int z = parts.Length == 2 ? 0 : int.Parse(parts[2]);
        return new Vector3(x, y, z);
    }

    private static long[] CalculateDistancesBetweenBeacons(ScannerData scannerData)
    {
        IReadOnlyList<Vector3> vectors = scannerData.Beacons;
        List<long> distances = new();

        for (int i = 0; i < vectors.Count; i++)
        {
            for (int j = i + 1; j < vectors.Count; j++)
            {
                float dist = Vector3.DistanceSquared(vectors[i], vectors[j]);
                distances.Add((long)dist);
            }
        }

        return distances.OrderBy(d => d).ToArray();
    }
}
