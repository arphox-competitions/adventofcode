﻿namespace AoC21.Day02;

internal sealed class Day02Solver : DaySolver
{
    internal override string Part1(AoCInput input)
    {
        int x = 0; // forward: +x
        int depth = 0;

        input.SplitByNewLines()
            .ForEach(str =>
            {
                string[] parts = str.Split();
                int change = int.Parse(parts[1]);
                string direction = parts[0];
                switch (direction)
                {
                    case "forward": x += change; break;
                    case "down": depth += change; break;
                    case "up": depth -= change; break;
                    default: throw new Exception();
                }
            });

        return (x * depth).ToString();
    }

    internal override string Part2(AoCInput input)
    {
        int x = 0; // forward: +x
        int depth = 0;
        int aim = 0;

        input.SplitByNewLines()
            .ForEach(str =>
            {
                string[] parts = str.Split();
                int change = int.Parse(parts[1]);
                string direction = parts[0];
                switch (direction)
                {
                    case "forward":
                        x += change;
                        depth += aim * change;
                        break;
                    case "down": aim += change; break;
                    case "up": aim -= change; break;
                    default: throw new Exception();
                }
            });

        return (x * depth).ToString();
    }
}
