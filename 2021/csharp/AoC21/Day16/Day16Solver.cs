﻿using System.Globalization;
using System.Text;

namespace AoC21.Day16;

internal sealed class Day16Solver : DaySolver
{
    internal override string Part1(AoCInput input)
    {
        string binaryString = ParseInput(input);
        long result = new Day16Parser(binaryString).RunPart1();
        return result.ToString();
    }

    internal override string Part2(AoCInput input)
    {
        string binaryString = ParseInput(input);
        long result = new Day16Parser(binaryString).RunPart2();
        return result.ToString();
    }

    private static string ParseInput(AoCInput input)
    {
        return new string(input.Value
            .Select(ch => int.Parse(ch.ToString(), NumberStyles.HexNumber)) // hex char to integer
            .Select(num => Convert.ToString(num, 2).PadLeft(4, '0'))
            .SelectMany(x => x)
            .ToArray());
    }
}

internal sealed class Day16Parser
{
    private readonly string _binaryString;
    private int _pointer = 0;
    private long _versionSum = 0;

    internal Day16Parser(string binaryString)
    {
        if (string.IsNullOrWhiteSpace(binaryString))
            throw new ArgumentException($"'{nameof(binaryString)}' cannot be null or whitespace.", nameof(binaryString));

        _binaryString = binaryString;
    }

    internal long RunPart1()
    {
        ParsePacket();
        return _versionSum;
    }

    internal long RunPart2()
    {
        Packet root = ParsePacket();
        return Evaluate(root);
    }

    private static long Evaluate(Packet packet)
    {
        return packet.Header.TypeId switch
        {
            0 => packet.Children.Sum(ch => Evaluate(ch)),
            1 => packet.Children.Product(ch => Evaluate(ch)),
            2 => packet.Children.Min(ch => Evaluate(ch)),
            3 => packet.Children.Max(ch => Evaluate(ch)),
            4 => (packet as LiteralPacket)!.Value,
            5 => Evaluate(packet.Children[0]) > Evaluate(packet.Children[1]) ? 1 : 0,  // less than
            6 => Evaluate(packet.Children[0]) < Evaluate(packet.Children[1]) ? 1 : 0,  // greater than
            7 => Evaluate(packet.Children[0]) == Evaluate(packet.Children[1]) ? 1 : 0, // equals
            _ => throw new InvalidOperationException(),
        };
    }

    private Packet ParsePacket()
    {
        byte version = Convert.ToByte(Take(3), 2);
        _pointer += 3;

        byte typeId = Convert.ToByte(Take(3), 2);
        _pointer += 3;

        Header header = new(version, typeId);

        Packet packet = typeId switch
        {
            4 => ParseLiteralPayload(header),
            _ => ParseOperatorPayload(header),
        };
        _versionSum += packet.Header.Version;
        return packet;
    }

    private Packet ParseLiteralPayload(Header header)
    {
        StringBuilder sb = new();
        bool repeat = true;
        while (repeat)
        {
            repeat = _binaryString[_pointer++] == '1';
            sb.Append(Take(4));
            _pointer += 4;
        }

        string str = sb.ToString();
        if (str.Length > 64) throw new Exception("long won't be enough");
        long literal = Convert.ToInt64(str, 2);

        return new LiteralPacket(header, literal);
    }

    private Packet ParseOperatorPayload(Header header)
    {
        char lengthTypeId = Take(1)[0]; _pointer++;
        return lengthTypeId switch
        {
            '0' => ProcessLenghType0OperatorPacket(header),
            '1' => ProcessLenghType1OperatorPacket(header),
            _ => throw new Exception("impossible"),
        };
    }

    private Packet ProcessLenghType0OperatorPacket(Header header)
    {
        Packet packet = new OperatorPacket(header);

        ushort totalLengthInBits = Convert.ToUInt16(Take(15), 2);
        _pointer += 15;
        int expectedPointerPosition = _pointer + totalLengthInBits;
        while (_pointer < expectedPointerPosition)
            packet.Children.Add(ParsePacket());

        return packet;
    }

    private Packet ProcessLenghType1OperatorPacket(Header header)
    {
        Packet packet = new OperatorPacket(header);

        ushort numOfSubPackets = Convert.ToUInt16(Take(11), 2);
        _pointer += 11;
        for (int i = 0; i < numOfSubPackets; i++)
            packet.Children.Add(ParsePacket());

        return packet;
    }

    private string Take(int bits) => _binaryString[_pointer..(_pointer + bits)];
}

internal readonly struct Header
{
    internal byte Version { get; }
    internal byte TypeId { get; }

    internal Header(byte version, byte typeId)
    {
        Version = version;
        TypeId = typeId;
    }

    public override string ToString()
    {
        string type = TypeId switch
        {
            0 => "sum",
            1 => "product",
            2 => "minimum",
            3 => "maximum",
            4 => "literal",
            5 => "greater than",
            6 => "less than",
            7 => "equals",
            _ => throw new InvalidOperationException(),
        };

        return $"Ver: {Version}, Type: {type} ({TypeId})";
    }
}

internal abstract record Packet
{
    internal Header Header { get; }
    internal List<Packet> Children { get; } = new();

    protected Packet(Header header) => Header = header;

    public override string ToString() => $"H:[{Header}]";
}

internal sealed record OperatorPacket : Packet
{
    internal OperatorPacket(Header header)
        : base(header)
    { }

    public override string ToString() => base.ToString(); // needed
}

internal sealed record LiteralPacket : Packet
{
    internal LiteralPacket(Header header, long value)
        : base(header)
        => Value = value;

    internal long Value { get; }

    public override string ToString() => $"{base.ToString()} - Value = {Value}";
}
