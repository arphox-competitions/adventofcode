﻿namespace AoC21.Day06;

internal sealed class Day06Solver : DaySolver
{
    internal override string Part1(AoCInput input)
    {
        List<int> fishes = input.IntegerCsv().ToList();

        const int DAY_COUNT = 80;
        for (int dayCounter = 0; dayCounter < DAY_COUNT; dayCounter++)
        {
            int fishesToAdd = 0;
            // Reduce days
            for (int i = 0; i < fishes.Count; i++)
            {
                if (fishes[i] == 0)
                {
                    fishes[i] = 6;
                    fishesToAdd++;
                }
                else
                {
                    fishes[i]--;
                }
            }
            // Add new fishes
            fishes.AddRange(Enumerable.Repeat(8, fishesToAdd));
        }

        return fishes.Count.ToString();
    }

    internal override string Part2(AoCInput input)
    {
        long[] fishCounts = new long[9];

        input.IntegerCsv()
            .ForEach(x => fishCounts[x]++);

        for (int dayCounter = 0; dayCounter < 256; dayCounter++)
        {
            long fishesToAdd = fishCounts[0];
            fishCounts[0] = fishCounts[1];
            fishCounts[1] = fishCounts[2];
            fishCounts[2] = fishCounts[3];
            fishCounts[3] = fishCounts[4];
            fishCounts[4] = fishCounts[5];
            fishCounts[5] = fishCounts[6];
            fishCounts[6] = fishCounts[7] + fishesToAdd;
            fishCounts[7] = fishCounts[8];
            fishCounts[8] = fishesToAdd;
        }

        return fishCounts.Sum().ToString();
    }
}
