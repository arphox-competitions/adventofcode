﻿namespace AoC21.Common
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = false, Inherited = false)]
    public sealed class IgnoreSolverAttribute : Attribute
    {
    }
}
