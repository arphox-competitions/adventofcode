﻿namespace AoC21.Common;

internal abstract class DaySolver
{
    private Lazy<string> InputLazy { get; }

    internal int Day { get; }
    internal abstract string Part1(AoCInput input);
    internal abstract string Part2(AoCInput input);

    internal string Part1Real() => Part1(InputLazy.Value);
    internal string Part2Real() => Part2(InputLazy.Value);

    public DaySolver()
    {
        Day = int.Parse(GetType().Name
            .Replace("Day", "")
            .Replace("Solver", ""));

        InputLazy = new Lazy<string>(() => GetRealInput());
    }

    private string GetRealInput()
        => File.ReadAllText($"_inputs/day{Day:d2}.txt");
}
