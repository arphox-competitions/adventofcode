﻿namespace AoC21.Common;

public static class ReflectionUtils
{
    public static bool HasCustomAttribute<TAttribute>(this Type type)
        => type.GetCustomAttributes(typeof(TAttribute), false).Length > 0;
}
