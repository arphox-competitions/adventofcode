﻿namespace AoC21.Common;

public static partial class EnumerableExtensions
{
    public static long Product(this IEnumerable<int> source)
    {
        long prod = 1;
        checked
        {
            foreach (int item in source)
                prod *= item;
        }
        return prod;
    }

    public static long Product<TSource>(this IEnumerable<TSource> source, Func<TSource, long> selector)
    {
        long prod = 1;
        checked
        {
            foreach (TSource item in source)
                prod *= selector(item);
        }
        return prod;
    }
}
