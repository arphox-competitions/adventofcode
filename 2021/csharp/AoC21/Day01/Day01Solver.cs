﻿namespace AoC21.Day01;

internal sealed class Day01Solver : DaySolver
{
    internal override string Part1(AoCInput input)
    {
        int[] values = input.IntegerArray();
        int counter = 0;
        for (int i = 1; i < values.Length; i++)
            if (values[i] > values[i - 1])
                counter++;

        return counter.ToString();
    }

    internal override string Part2(AoCInput input)
    {
        int[] values = input.IntegerArray();
        int counter = 0;
        int windowSum = values[..3].Sum();
        for (int i = 0; i < values.Length - 3; i++)
        {
            int newWindowSum = windowSum - values[i] + values[i + 3];

            if (newWindowSum > windowSum)
                counter++;

            windowSum = newWindowSum;
        }

        return counter.ToString();
    }
}
