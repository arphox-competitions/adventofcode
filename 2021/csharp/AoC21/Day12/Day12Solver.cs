﻿namespace AoC21.Day12;

internal sealed class Day12Solver : DaySolver
{
    internal override string Part1(AoCInput input)
    {
        var graph = ParseGraph(input.SplitByNewLines());
        int count = new Part1Impl(graph).FindRouteCount();
        return count.ToString();
    }

    internal override string Part2(AoCInput input)
    {
        var graph = ParseGraph(input.SplitByNewLines());
        int count = new Part2Impl(graph).FindRouteCount();
        return count.ToString();
    }

    private static Graph ParseGraph(string[] lines)
    {
        GraphParser parser = new();
        lines.ForEach(x => parser.AddEntry(x));
        return parser.GetGraph();
    }
}
