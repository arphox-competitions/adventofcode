﻿namespace AoC21.Day12;

internal sealed class Part2Impl
{
    private readonly Graph _graph;
    private readonly HashSet<string> _routes = new();

    public Part2Impl(Graph graph)
        => _graph = graph ?? throw new ArgumentNullException(nameof(graph));

    internal int FindRouteCount()
    {
        List<Node> visitedNodes = new();
        visitedNodes.Add(_graph.Start);
        Process(_graph.Start, visitedNodes, true);
        return _routes.Count;
    }

    private void Process(Node current, List<Node> visitedNodes, bool dualVisitAllowed)
    {
        var visitedSmallNodes = visitedNodes.Where(n => n.IsSmallCave).ToArray();

        IReadOnlyList<Node> neighborsToVisit = current.Neighbors
            .Except(visitedSmallNodes)
            .ToList();

        if (!dualVisitAllowed)
        {
            foreach (Node neighbor in neighborsToVisit)
                ProcessNode(neighbor, visitedNodes.ToList(), false);
            return;
        }

        // Branch 1: allow dual visit, but later
        foreach (Node neighbor in neighborsToVisit)
            ProcessNode(neighbor, visitedNodes.ToList(), true);

        // Branch 2: duplicate now
        var smallNeighborCaves = current.Neighbors.Where(x => x.IsSmallCave);
        var cavesToDuplicate = smallNeighborCaves.Where(x =>
            visitedSmallNodes.Contains(x) && // it only becomes a duplicate if we already visited it
            x != _graph.Start &&
            x != _graph.End);

        foreach (Node item in cavesToDuplicate)
        {
            var newNeighborsToVisit = neighborsToVisit.ToList();
            newNeighborsToVisit.Add(item);
            foreach (Node neighbor in newNeighborsToVisit)
                ProcessNode(neighbor, visitedNodes.ToList(), false);
        }
    }

    private void ProcessNode(Node node, List<Node> visitedNodes, bool dualVisitAllowed)
    {
        visitedNodes.Add(node);

        if (node == _graph.End)
        {
            string routeString = string.Join(',', visitedNodes.Select(n => n.Label));
            _routes.Add(routeString);
        }
        else
        {
            Process(node, new List<Node>(visitedNodes), dualVisitAllowed);
        }
    }
}
