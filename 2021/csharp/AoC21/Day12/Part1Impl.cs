﻿namespace AoC21.Day12;

internal sealed class Part1Impl
{
    private readonly Graph _graph;
    private int _routeCount = 0;

    public Part1Impl(Graph graph)
        => _graph = graph ?? throw new ArgumentNullException(nameof(graph));

    internal int FindRouteCount()
    {
        ProcessNode(_graph.Start, new HashSet<Node>() { _graph.Start });
        return _routeCount;
    }

    private void ProcessNode(Node current, HashSet<Node> excludedNodes)
    {
        foreach (Node neighbor in current.Neighbors)
        {
            if (excludedNodes.Contains(neighbor))
                continue;

            if (neighbor == _graph.End)
            {
                _routeCount++;
            }
            else
            {
                List<Node> newPathExcludedNodes = new(excludedNodes);
                if (neighbor.IsSmallCave)
                    newPathExcludedNodes.Add(neighbor);
                ProcessNode(neighbor, new HashSet<Node>(newPathExcludedNodes));
            }
        }
    }
}
