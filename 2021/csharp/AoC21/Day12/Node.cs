﻿namespace AoC21.Day12;

internal sealed class Node
{
    internal string Label { get; }
    internal bool IsLargeCave { get; }
    internal bool IsSmallCave => !IsLargeCave;
    internal IReadOnlyList<Node> Neighbors => _neighbors;

    private readonly List<Node> _neighbors = new List<Node>();

    internal Node(string label)
    {
        if (string.IsNullOrWhiteSpace(label))
            throw new ArgumentException($"'{nameof(label)}' cannot be null or whitespace.", nameof(label));

        Label = label;
        IsLargeCave = char.IsUpper(label[0]);
    }

    internal void AddNeighbor(Node other)
        => _neighbors.Add(other);

    public override string ToString() =>
        $"{Label} -> {string.Join(",", _neighbors.Select(n => n.Label))}";
}
