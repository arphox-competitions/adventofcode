﻿using AoC21.Day11;

namespace AoC21.Test.Day11;

/// <summary>
/// Unit tests for <see cref="Day11Solver"/>
/// </summary>
public sealed class Day11SolverTests
{
    [Theory]
    [InlineData(Example1, "1656")]
    [InlineData(RealInput, "1681")]
    public void Part1(string input, string expectedResult)
    {
        string result = new Day11Solver().Part1(input);
        result.Should().Be(expectedResult);
    }

    [Theory]
    [InlineData(Example1, "195")]
    [InlineData(RealInput, "276")]
    public void Part2(string input, string expectedResult)
    {
        string result = new Day11Solver().Part2(input);
        result.Should().Be(expectedResult);
    }

    #region [ Data ]

    private const string Example1 =
@"5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";

    private const string RealInput =
@"6227618536
2368158384
5385414113
4556757523
6746486724
4881323884
4648263744
4871332872
4724128228
4316512167";

    #endregion
}
