﻿using AoC21.Day12;

namespace AoC21.Test.Day12;

/// <summary>
/// Unit tests for <see cref="Day12Solver"/>
/// </summary>
public sealed class Day12SolverTests
{
    [Theory]
    [InlineData(Example1, "10")]
    [InlineData(Example2, "19")]
    [InlineData(Example3, "226")]
    [InlineData(RealInput, "4167")]
    public void Part1(string input, string expectedResult)
    {
        string result = new Day12Solver().Part1(input);
        result.Should().Be(expectedResult);
    }

    [Theory]
    [InlineData(Example1, "36")]
    [InlineData(Example2, "103")]
    [InlineData(Example3, "3509")]
    [InlineData(RealInput, "98441")]
    public void Part2(string input, string expectedResult)
    {
        string result = new Day12Solver().Part2(input);
        result.Should().Be(expectedResult);
    }

    #region [ Data ]

    private const string Example1 =
@"start-A
start-b
A-c
A-b
b-d
A-end
b-end";

    private const string Example2 =
@"dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc";

    private const string Example3 =
@"fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW";

    private const string RealInput =
@"xx-xh
vx-qc
cu-wf
ny-LO
cu-DR
start-xx
LO-vx
cu-LO
xx-cu
cu-ny
xh-start
qc-DR
vx-AP
end-LO
ny-DR
vx-end
DR-xx
start-DR
end-ny
ny-xx
xh-DR
cu-xh";

    #endregion
}
