﻿using AoC21.Day23;

namespace AoC21.Test.Day23;

/// <summary>
/// Unit tests for <see cref="Day23Solver"/>
/// </summary>
public sealed class Day23SolverTests
{
    [Theory]
    [InlineData(Example1, "xxxx")]
    [InlineData(RealInput, "xxxx")]
    public void Part1(string input, string expectedResult)
    {
        string result = new Day23Solver().Part1(input);
        result.Should().Be(expectedResult);
    }

    [Theory]
    [InlineData(Example1, "xxxx")]
    [InlineData(RealInput, "xxxx")]
    public void Part2(string input, string expectedResult)
    {
        string result = new Day23Solver().Part2(input);
        result.Should().Be(expectedResult);
    }

    #region [ Data ]

    private const string Example1 =
@"PLACEHOLDER1";

    private const string RealInput =
@"PLACEHOLDER2";

    #endregion
}
