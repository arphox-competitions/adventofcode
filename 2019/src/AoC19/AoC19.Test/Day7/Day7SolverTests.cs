﻿using AoC19.Day7;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace AoC19.Test.Day7
{
    public sealed class Day7SolverTests
    {
        private readonly ITestOutputHelper _testOutput;

        public Day7SolverTests(ITestOutputHelper testOutput)
        {
            _testOutput = testOutput;
        }

        [Theory]
        [InlineData(43210, "3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0")]
        [InlineData(54321, "3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0")]
        [InlineData(65210, "3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0")]
        public void Part1WebsiteExamples(long expectedOutput, string input)
        {
            var solver = new Day7Solver(input);
            (long result, int[] phaseSetting) = solver.SolvePart1();
            _testOutput.WriteLine(string.Join(", ", phaseSetting));
            result.Should().Be(expectedOutput);
        }

        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day7Solver(Day7Solver.ReadRealInput());
            (long result, _) = solver.SolvePart1();
            result.Should().Be(51679);
        }

        [Theory]
        [InlineData(139629729, new int[] { 9, 8, 7, 6, 5 }, "3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5")]
        [InlineData(18216, new int[] { 9, 7, 8, 5, 6 }, "3,52,1001,52,-5,52,3,53,1,52,56,54,1007,54,5,55,1005,55,26,1001,54,-5,54,1105,1,12,1,53,54,53,1008,54,0,55,1001,55,1,55,2,53,55,53,4,53,1001,56,-1,56,1005,56,6,99,0,0,0,0,10")]
        public void Part2WebsiteExamples(long expectedOutput, int[] expectedPhaseSetting, string input)
        {
            var solver = new Day7Solver(input);
            (long result, int[] phaseSetting) = solver.SolvePart2();

            result.Should().Be(expectedOutput);
            phaseSetting.Should().BeEquivalentTo(expectedPhaseSetting);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day7Solver(Day7Solver.ReadRealInput());
            (long result, _) = solver.SolvePart2();
            result.Should().Be(19539216);
        }
    }
}