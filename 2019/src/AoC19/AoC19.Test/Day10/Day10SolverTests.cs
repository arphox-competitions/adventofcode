﻿using AoC19.Day10;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace AoC19.Test.Day10
{
    public sealed class Day10SolverTests
    {
        private readonly ITestOutputHelper _testOutput;

        public Day10SolverTests(ITestOutputHelper testOutput)
        {
            _testOutput = testOutput;
        }

        [Fact]
        public void Part1WebsiteExample1()
        {
            const string input =
                                ".#..#" + "\n" +
                                "....." + "\n" +
                                "#####" + "\n" +
                                "....#" + "\n" +
                                "...##";

            var solver = new Day10Solver(input.Split('\n'));
            (int x, int y, int asteroids) = solver.SolvePart1();
            x.Should().Be(3);
            y.Should().Be(4);
            asteroids.Should().Be(8);
        }

        [Fact]
        public void Part1WebsiteExample2()
        {
            const string input =
                                "......#.#." + "\n" +
                                "#..#.#...." + "\n" +
                                "..#######." + "\n" +
                                ".#.#.###.." + "\n" +
                                ".#..#....." + "\n" +
                                "..#....#.#" + "\n" +
                                "#..#....#." + "\n" +
                                ".##.#..###" + "\n" +
                                "##...#..#." + "\n" +
                                ".#....####";

            var solver = new Day10Solver(input.Split('\n'));
            (int x, int y, int asteroids) = solver.SolvePart1();
            x.Should().Be(5);
            y.Should().Be(8);
            asteroids.Should().Be(33);
        }

        [Fact]
        public void Part1WebsiteExample3()
        {
            const string input =
                                "#.#...#.#." + "\n" +
                                ".###....#." + "\n" +
                                ".#....#..." + "\n" +
                                "##.#.#.#.#" + "\n" +
                                "....#.#.#." + "\n" +
                                ".##..###.#" + "\n" +
                                "..#...##.." + "\n" +
                                "..##....##" + "\n" +
                                "......#..." + "\n" +
                                ".####.###.";

            var solver = new Day10Solver(input.Split('\n'));
            (int x, int y, int asteroids) = solver.SolvePart1();
            x.Should().Be(1);
            y.Should().Be(2);
            asteroids.Should().Be(35);
        }

        [Fact]
        public void Part1WebsiteExample4()
        {
            const string input =
                                ".#..#..###" + "\n" +
                                "####.###.#" + "\n" +
                                "....###.#." + "\n" +
                                "..###.##.#" + "\n" +
                                "##.##.#.#." + "\n" +
                                "....###..#" + "\n" +
                                "..#.#..#.#" + "\n" +
                                "#..#.#.###" + "\n" +
                                ".##...##.#" + "\n" +
                                ".....#.#..";

            var solver = new Day10Solver(input.Split('\n'));
            (int x, int y, int asteroids) = solver.SolvePart1();
            x.Should().Be(6);
            y.Should().Be(3);
            asteroids.Should().Be(41);
        }

        [Fact]
        public void Part1WebsiteExample5()
        {
            const string input =
                                ".#..##.###...#######" + "\n" +
                                "##.############..##." + "\n" +
                                ".#.######.########.#" + "\n" +
                                ".###.#######.####.#." + "\n" +
                                "#####.##.#.##.###.##" + "\n" +
                                "..#####..#.#########" + "\n" +
                                "####################" + "\n" +
                                "#.####....###.#.#.##" + "\n" +
                                "##.#################" + "\n" +
                                "#####.##.###..####.." + "\n" +
                                "..######..##.#######" + "\n" +
                                "####.##.####...##..#" + "\n" +
                                ".#####..#.######.###" + "\n" +
                                "##...#.##########..." + "\n" +
                                "#.##########.#######" + "\n" +
                                ".####.#.###.###.#.##" + "\n" +
                                "....##.##.###..#####" + "\n" +
                                ".#.#.###########.###" + "\n" +
                                "#.#.#.#####.####.###" + "\n" +
                                "###.##.####.##.#..##";

            var solver = new Day10Solver(input.Split('\n'));
            (int x, int y, int asteroids) = solver.SolvePart1();
            x.Should().Be(11);
            y.Should().Be(13);
            asteroids.Should().Be(210);
        }

        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day10Solver(Day10Solver.ReadRealInput());
            (int x, int y, int asteroids) = solver.SolvePart1();
            x.Should().Be(20);
            y.Should().Be(21);
            asteroids.Should().Be(247);
        }

        [Fact]
        public void Part2WebsiteExample()
        {
            const string input =
                                ".#..##.###...#######" + "\n" +
                                "##.############..##." + "\n" +
                                ".#.######.########.#" + "\n" +
                                ".###.#######.####.#." + "\n" +
                                "#####.##.#.##.###.##" + "\n" +
                                "..#####..#.#########" + "\n" +
                                "####################" + "\n" +
                                "#.####....###.#.#.##" + "\n" +
                                "##.#################" + "\n" +
                                "#####.##.###..####.." + "\n" +
                                "..######..##.#######" + "\n" +
                                "####.##.####...##..#" + "\n" +
                                ".#####..#.######.###" + "\n" +
                                "##...#.##########..." + "\n" +
                                "#.##########.#######" + "\n" +
                                ".####.#.###.###.#.##" + "\n" +
                                "....##.##.###..#####" + "\n" +
                                ".#.#.###########.###" + "\n" +
                                "#.#.#.#####.####.###" + "\n" +
                                "###.##.####.##.#..##";

            var solver = new Day10Solver(input.Split('\n'));

            PointWithAngle[] pointsWithAngle = solver.Part2_GetPointWithAngles();

            pointsWithAngle[0].X.Should().Be(11);
            pointsWithAngle[0].Y.Should().Be(12);

            pointsWithAngle[1].X.Should().Be(12);
            pointsWithAngle[1].Y.Should().Be(1);

            pointsWithAngle[2].X.Should().Be(12);
            pointsWithAngle[2].Y.Should().Be(2);

            pointsWithAngle[9].X.Should().Be(12);
            pointsWithAngle[9].Y.Should().Be(8);

            pointsWithAngle[19].X.Should().Be(16);
            pointsWithAngle[19].Y.Should().Be(0);

            pointsWithAngle[49].X.Should().Be(16);
            pointsWithAngle[49].Y.Should().Be(9);

            pointsWithAngle[99].X.Should().Be(10);
            pointsWithAngle[99].Y.Should().Be(16);

            pointsWithAngle[198].X.Should().Be(9);
            pointsWithAngle[198].Y.Should().Be(6);

            pointsWithAngle[199].X.Should().Be(8);
            pointsWithAngle[199].Y.Should().Be(2);

            pointsWithAngle[200].X.Should().Be(10);
            pointsWithAngle[200].Y.Should().Be(9);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day10Solver(Day10Solver.ReadRealInput());
            int result = solver.SolvePart2();
            result.Should().Be(1919);
        }
    }
}