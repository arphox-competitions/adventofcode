﻿using AoC19.Day10;
using FluentAssertions;
using Xunit;

namespace AoC19.Test.Day10
{
    public sealed class Day10MathHelperTests
    {
        [Theory]
        [InlineData(0, -1, 0)]
        [InlineData(1, -3, 18.43)]
        [InlineData(1, -1, 45)]
        [InlineData(3, -1, 71.57)]
        [InlineData(1, 0, 90)]
        [InlineData(3, 1, 108.43)]
        [InlineData(1, 1, 135)]
        [InlineData(1, 3, 161.57)]
        [InlineData(0, 1, 180)]
        [InlineData(-1, 4, 194.04)]
        [InlineData(-1, 1, 225)]
        [InlineData(-5, 1, 258.69)]
        [InlineData(-1, 0, 270)]
        [InlineData(-5, -2, 291.80)]
        [InlineData(-5, -4, 308.66)]
        [InlineData(-1, -1, 315)]
        [InlineData(-2, -5, 338.20)]
        public void GetAngleReturnsCorrectAngle(int dirX, int dirY, double expectedAngle)
        {
            double actualAngle = Day10MathHelper.GetAngle(dirX, dirY);
            actualAngle.Should().BeApproximately(expectedAngle, 0.1);
        }
    }
}