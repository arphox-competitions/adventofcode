﻿using AoC19.Day13;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace AoC19.Test.Day13
{
    public sealed class Day13SolverTests
    {
        private readonly ITestOutputHelper _testOutput;

        public Day13SolverTests(ITestOutputHelper testOutput)
        {
            _testOutput = testOutput;
        }

        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day13Solver(Day13Solver.ReadRealInput());
            var result = solver.SolvePart1();
            result.Should().Be(312);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day13Solver(Day13Solver.ReadRealInput());
            var result = solver.SolvePart2();
            result.Should().Be(15909);
        }
    }
}