﻿using AoC19.Day6;
using FluentAssertions;
using Xunit;

namespace AoC19.Test.Day6
{
    public sealed class Day6SolverTests
    {
        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day6Solver(Day6Solver.ReadRealInput());
            int result = solver.SolvePart1();
            result.Should().Be(247089);
        }

        [Fact]
        public void Part1WebsiteExample()
        {
            const string map = "COM)B\n" +
                               "B)C\n" +
                               "C)D\n" +
                               "D)E\n" +
                               "E)F\n" +
                               "B)G\n" +
                               "G)H\n" +
                               "D)I\n" +
                               "E)J\n" +
                               "J)K\n" +
                               "K)L";

            var solver = new Day6Solver(map.Split('\n'));
            int result = solver.SolvePart1();
            result.Should().Be(42);
        }

        [Fact]
        public void Part2WebsiteExample()
        {
            const string map = "COM)B,B)C,C)D,D)E,E)F,B)G,G)H,D)I,E)J,J)K,K)L,K)YOU,I)SAN";

            var solver = new Day6Solver(map.Split(','));
            int result = solver.SolvePart2();
            result.Should().Be(4);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day6Solver(Day6Solver.ReadRealInput());
            int result = solver.SolvePart2();
            result.Should().Be(442);
        }
    }
}