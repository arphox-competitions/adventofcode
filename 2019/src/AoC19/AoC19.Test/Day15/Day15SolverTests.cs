﻿using AoC19.Day15;
using FluentAssertions;
using Xunit;
using Xunit.Abstractions;

namespace AoC19.Test.Day15
{
    public sealed class Day15SolverTests
    {
        private readonly ITestOutputHelper _testOutput;

        public Day15SolverTests(ITestOutputHelper testOutput)
        {
            _testOutput = testOutput;
        }

        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day15Solver(Day15Solver.ReadRealInput(), shouldPrint: false);
            int result = solver.SolvePart1();
            result.Should().Be(246);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day15Solver(Day15Solver.ReadRealInput(), shouldPrint: false);
            int result = solver.SolvePart2();
            result.Should().Be(376);
        }
    }
}