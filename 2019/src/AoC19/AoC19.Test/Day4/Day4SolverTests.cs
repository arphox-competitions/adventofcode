﻿using AoC19.Day4;
using FluentAssertions;
using Xunit;

namespace AoC19.Test.Day4
{
    public sealed class Day4SolverTests
    {
        [Fact]
        public void Part1RealInput()
        {
            var solver = new Day4Solver(Day4Solver.ReadRealInput());
            int result = solver.SolvePart1();
            result.Should().Be(1767);
        }

        [Fact]
        public void Part2RealInput()
        {
            var solver = new Day4Solver(Day4Solver.ReadRealInput());
            int result = solver.SolvePart2();
            result.Should().Be(1192);
        }

        [Theory]
        [InlineData(112233, true)]
        [InlineData(123444, false)]
        [InlineData(111122, true)]
        [InlineData(123499, true)]
        public void Part2_OneNumberTest(int number, bool expectedResult)
        {
            var solver = new Day4Solver(Day4Solver.ReadRealInput());
            bool actualResult = solver.Part2_MeetCriteria(number);
            actualResult.Should().Be(expectedResult);
        }
    }
}