﻿using System;
using System.IO;
using System.Linq;

namespace AoC19.Day12
{
    public sealed class Day12Solver
    {
        private const string InputPath = @"_inputs/day12input.txt";
        public static string[] ReadRealInput() => File.ReadAllLines(InputPath);

        private readonly Vector3[] _initialPositions;
        private Vector3[] _positions;
        private Vector3[] _velocities;

        public Day12Solver(string[] input)
        {
            _initialPositions = input
                .Select(Vector3.Parse)
                .ToArray();

            Reset();
        }

        private void Reset()
        {
            _positions = _initialPositions.ToArray();
            _velocities = _positions.Select(_ => new Vector3()).ToArray();
        }

        public int CalculateTotalSystemEnergy()
        {
            int sum = 0;

            for (int i = 0; i < _positions.Length; i++)
            {
                int potentialEnergy = _positions[i].CalculateEnergy();
                int kineticEnergy = _velocities[i].CalculateEnergy();
                int moonEnergy = potentialEnergy * kineticEnergy;
                sum += moonEnergy;
            }

            return sum;
        }

        public void P1_DoOneIteration()
        {
            P1_UpdateVelocitiesByApplyingGravity();
            P1_UpdatePositionsByApplyingVelocity();
        }

        private void P1_UpdateVelocitiesByApplyingGravity()
        {
            for (int a = 0; a < _positions.Length; a++)
            {
                for (int b = a + 1; b < _positions.Length; b++)
                {
                    P1_ApplyXGravityForMoons(a, b);
                    P1_ApplyYGravityForMoons(a, b);
                    P1_ApplyZGravityForMoons(a, b);
                }
            }
        }

        private void P1_ApplyXGravityForMoons(int moon1Index, int moon2Index)
        {
            if (_positions[moon1Index].X > _positions[moon2Index].X)
            {
                _velocities[moon1Index].X--;
                _velocities[moon2Index].X++;
            }
            else if (_positions[moon1Index].X < _positions[moon2Index].X)
            {
                _velocities[moon1Index].X++;
                _velocities[moon2Index].X--;
            }
        }

        private void P1_ApplyYGravityForMoons(int moon1Index, int moon2Index)
        {
            if (_positions[moon1Index].Y > _positions[moon2Index].Y)
            {
                _velocities[moon1Index].Y--;
                _velocities[moon2Index].Y++;
            }
            else if (_positions[moon1Index].Y < _positions[moon2Index].Y)
            {
                _velocities[moon1Index].Y++;
                _velocities[moon2Index].Y--;
            }
        }

        private void P1_ApplyZGravityForMoons(int moon1Index, int moon2Index)
        {
            if (_positions[moon1Index].Z > _positions[moon2Index].Z)
            {
                _velocities[moon1Index].Z--;
                _velocities[moon2Index].Z++;
            }
            else if (_positions[moon1Index].Z < _positions[moon2Index].Z)
            {
                _velocities[moon1Index].Z++;
                _velocities[moon2Index].Z--;
            }
        }

        private void P1_UpdatePositionsByApplyingVelocity()
        {
            for (int i = 0; i < _positions.Length; i++)
            {
                _positions[i].AddToThis(_velocities[i]);
            }
        }

        public int SolvePart1()
        {
            for (int steps = 0; steps < 1000; steps++)
                P1_DoOneIteration();

            return CalculateTotalSystemEnergy();
        }

        public long SolvePart2()
        {
            int iterationsX = FindXRepetition();
            Reset();
            int iterationsY = FindYRepetition();
            Reset();
            int iterationsZ = FindZRepetition();

            long result = LCM3(iterationsX, iterationsY, iterationsZ);
            return result;
        }

        private int FindXRepetition()
        {
            int[] initialXcoords = _positions.Select(p => p.X).ToArray();
            int[] newXcoords = new int[initialXcoords.Length];
            int iterationsX = 0;
            while (!Enumerable.SequenceEqual(newXcoords, initialXcoords))
            {
                P1_DoOneIteration();
                newXcoords = _positions.Select(p => p.X).ToArray();
                iterationsX++;
            }
            iterationsX++; // magic
            return iterationsX;
        }

        private int FindYRepetition()
        {
            int[] initialYcoords = _positions.Select(p => p.Y).ToArray();
            int[] newYcoords = new int[initialYcoords.Length];
            int iterationsY = 0;
            while (!Enumerable.SequenceEqual(newYcoords, initialYcoords))
            {
                P1_DoOneIteration();
                newYcoords = _positions.Select(p => p.Y).ToArray();
                iterationsY++;
            }
            iterationsY++; // magic
            return iterationsY;
        }

        private int FindZRepetition()
        {
            int[] initialZcoords = _positions.Select(p => p.Z).ToArray();
            int[] newZcoords = new int[initialZcoords.Length];
            int iterationsZ = 0;
            while (!Enumerable.SequenceEqual(newZcoords, initialZcoords))
            {
                P1_DoOneIteration();
                newZcoords = _positions.Select(p => p.Z).ToArray();
                iterationsZ++;
            }
            iterationsZ++; // magic
            return iterationsZ;
        }

        public string[] GetStateDesignator()
        {
            string[] output = new string[_positions.Length];

            for (int i = 0; i < _positions.Length; i++)
            {
                var pos = _positions[i];
                var vel = _velocities[i];

                output[i] =
                    $"pos=<x={pos.X}, y={pos.Y}, z={pos.Z}>, " +
                    $"vel=<x={vel.X}, y={vel.Y}, z={vel.Z}>, ";
            }

            return output;
        }

        private static long LCM3(long a, long b, long c) => LCM2(a, LCM2(b, c));
        private static long LCM2(long a, long b) => Math.Abs(a * b) / GCD2(a, b);
        private static long GCD2(long a, long b) => b == 0 ? a : GCD2(b, a % b);
    }
}