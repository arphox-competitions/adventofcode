﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace AoC19.Day14
{
    class Day14Part2Solver
    {
        private readonly string[] _input;
        private readonly Dictionary<string, Recipe> _recipes;
        private readonly StockManager _stockManager;
        private long _craftedFuels = 0;
        private bool _outOfOre = false;

        public Day14Part2Solver(string[] input)
        {
            _input = input;
            var recipesList = Day14InputParser.ParseInput(input);
            _recipes = recipesList.ToDictionary(key => key.Material);
            _stockManager = new StockManager();
        }

        public long Solve()
        {
            long scale = 100_000;
            long guess = 1;

            long result = 0;
            while (scale > 1)
            {
                result = new Day14Part2Solver(_input).Solve(guess, scale);
                guess = result - scale;
                scale /= 10;
            }

            guess = result - 10;
            result = new Day14Part2Solver(_input).Solve(guess, scale);

            return result - 1;
        }

        private long Solve(long guess, long scale)
        {
            _stockManager.Put("ORE", 1_000_000_000_000); // 1 trillion ORE
            Craft(guess, "FUEL");

            while (!_outOfOre)
            {
                Craft(scale, "FUEL");
            }

            return _craftedFuels;
        }

        private void Craft(long requiredAmount, string material)
        {
            if (material == "FUEL")
                _craftedFuels += requiredAmount;

            requiredAmount = _stockManager.DecreaseWithStockValue(material, requiredAmount);
            if (requiredAmount == 0)
                return;

            if (material == "ORE") // and we could not serve from stock
            {
                _outOfOre = true;
                return;
            }

            Recipe recipe = _recipes[material];
            long recipeRepeatCount = (long)Math.Ceiling(requiredAmount / (double)recipe.Amount);
            recipeRepeatCount = Math.Max(1, recipeRepeatCount);

            foreach (Ingredient ingredient in recipe.Ingredients)
                Craft(ingredient.Amount * recipeRepeatCount, ingredient.Material);

            long waste = recipe.Amount * recipeRepeatCount - requiredAmount;
            _stockManager.Put(material, waste);
        }
    }
}
