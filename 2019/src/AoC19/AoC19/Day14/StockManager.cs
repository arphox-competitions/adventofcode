﻿using System;
using System.Collections.Generic;

namespace AoC19.Day14
{
    public sealed class StockManager
    {
        private readonly Dictionary<string, long> _stock = new Dictionary<string, long>();

        public long this[string material]
        {
            get
            {
                _stock.TryGetValue(material, out long storedAmount);
                return storedAmount;
            }
        }

        public void Put(string material, long amount)
        {
            if (amount < 0) throw new ArgumentOutOfRangeException(nameof(amount), amount, "Value cannot be negative");
            if (amount == 0) return;

            long storedAmount = this[material];
            _stock[material] = storedAmount + amount;
        }

        public long DecreaseWithStockValue(string material, long desiredAmount)
        {
            long storedAmount = this[material];
            _stock[material] = Math.Max(storedAmount - desiredAmount, 0);
            return Math.Max(desiredAmount - storedAmount, 0);
        }
    }
}