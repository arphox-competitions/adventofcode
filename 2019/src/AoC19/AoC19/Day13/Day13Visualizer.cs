﻿using System;

namespace AoC19.Day13
{
    public sealed class Day13Visualizer
    {
        public void Run()
        {
            Day13Solver solver = new Day13Solver(Day13Solver.ReadRealInput());
            solver.SolvePart2(true);
        }
    }
}