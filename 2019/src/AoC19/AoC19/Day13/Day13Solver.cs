﻿using AoC19.Common.IntCode;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace AoC19.Day13
{
    public sealed class Day13Solver
    {
        private const string InputPath = @"_inputs/day13input.txt";
        public static string ReadRealInput() => File.ReadAllLines(InputPath).Single();

        private readonly string _input;

        public Day13Solver(string input)
        {
            _input = input;
        }

        public int SolvePart1()
        {
            IntCodeVM vm = new IntCodeVM(_input);
            HaltType haltType = vm.Run();
            if (haltType != HaltType.Terminated) { Debugger.Break(); throw new Exception(); }
            if (vm.Output.Count % 3 != 0) { Debugger.Break(); throw new Exception(); }

            Dictionary<(long x, long y), long> map = new Dictionary<(long x, long y), long>();
            while (vm.Output.Any())
            {
                long x = vm.Output.Dequeue();
                long y = vm.Output.Dequeue();
                long tileId = vm.Output.Dequeue();
                map[(x, y)] = tileId;
            }

            int numberOfBlocks = map.Values.Count(v => v == 2);
            return numberOfBlocks;
        }

        public int SolvePart2(bool display = false)
        {
            if (display) Console.CursorVisible = false;
            IntCodeVM vm = new IntCodeVM(_input);
            vm.Memory[0] = 2; // play free

            Dictionary<(int x, int y), TileType> map = new Dictionary<(int x, int y), TileType>();
            (int x, int y) ballPos = (0, 0);
            (int x, int y) paddlePos = (0, 0);
            int currentScore = 0;

            HaltType haltType = vm.Run();
            while (haltType != HaltType.Terminated)
            {
                ProcessOutput(vm, map, ref ballPos, ref paddlePos, ref currentScore, display);

                JoystickTilt command = JoystickTilt.Neutral;
                if (ballPos.x > paddlePos.x)
                    command = JoystickTilt.Right;
                else if (ballPos.x < paddlePos.x)
                    command = JoystickTilt.Left;

                vm.Input.Enqueue((int)command);
                haltType = vm.Run();
            }

            ProcessOutput(vm, map, ref ballPos, ref paddlePos, ref currentScore, display);
            return currentScore;
        }

        private void ProcessOutput(
            IntCodeVM vm,
            Dictionary<(int x, int y), TileType> map,
            ref (int x, int y) ballPos,
            ref (int x, int y) paddlePos,
            ref int currentScore,
            bool display)
        {
            while (vm.Output.Any())
            {
                int x = (int)vm.Output.Dequeue();
                int y = (int)vm.Output.Dequeue();
                int thirdOutput = (int)vm.Output.Dequeue();
                if (x == -1 && y == 0)
                {
                    currentScore = thirdOutput;
                    continue;
                }

                TileType tileType = (TileType)thirdOutput;
                map[(x, y)] = tileType;

                if (tileType == TileType.Ball) ballPos = (x, y);
                if (tileType == TileType.Paddle) paddlePos = (x, y);

                if (display) PrintTile(x, y, tileType);
            }
            if (display) PrintScore(currentScore);
        }

        private void PrintScore(int currentScore)
        {
            Console.SetCursorPosition(0, 0);
            Console.BackgroundColor = ConsoleColor.Black;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write($"score: {currentScore}");
        }

        private void PrintTile(int x, int y, TileType tileType)
        {
            Console.SetCursorPosition(x, y);

            char tileChar = '?';
            switch (tileType)
            {
                case TileType.Empty: tileChar = ' '; Console.BackgroundColor = ConsoleColor.Black; break;
                case TileType.Wall: tileChar = ' '; Console.BackgroundColor = ConsoleColor.DarkYellow; break;
                case TileType.Block: tileChar = ' '; Console.BackgroundColor = ConsoleColor.Red; break;
                case TileType.Paddle: tileChar = ' '; Console.BackgroundColor = ConsoleColor.Green; break;
                case TileType.Ball: tileChar = ' '; Console.BackgroundColor = ConsoleColor.White; break;
            }

            Console.Write(tileChar);
            Console.ResetColor();
        }
    }
}