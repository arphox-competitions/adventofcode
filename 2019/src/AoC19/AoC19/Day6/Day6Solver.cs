﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AoC19.Day6
{
    public sealed class Day6Solver
    {
        private const string InputPath = @"_inputs/day6input.txt";
        public static string[] ReadRealInput() => File.ReadAllLines(InputPath);
        private readonly (string, string)[] _rawOrbitData;
        private readonly Dictionary<string, List<string>> _orbitList;

        public Day6Solver(string[] input)
        {
            _rawOrbitData = input
                .Select(line =>
                {
                    string[] parts = line.Split(')');
                    return (parts[0], parts[1]);
                })
                .ToArray();

            _orbitList = CreateOrbitData(input);
        }

        public int SolvePart1()
        {
            int fullCount = 0;
            foreach (KeyValuePair<string, List<string>> item in _orbitList)
            {
                fullCount += CountChildrenOfTarget(_orbitList, item.Key);
            }

            return fullCount;
        }

        private Dictionary<string, List<string>> CreateOrbitData(string[] input)
        {
            Dictionary<string, List<string>> orbitList = new Dictionary<string, List<string>>(_rawOrbitData.Length);
            foreach ((string reference, string orbiter) in _rawOrbitData)
            {
                if (orbitList.TryGetValue(reference, out List<string> list))
                    list.Add(orbiter);
                else
                    orbitList.Add(reference, new List<string>() { orbiter });
            }

            return orbitList;
        }

        private int CountChildrenOfTarget(Dictionary<string, List<string>> orbitList, string target)
        {
            int sum = 0;

            if (orbitList.TryGetValue(target, out List<string> values))
            {
                sum += values.Count;

                foreach (string item in values)
                {
                    sum += CountChildrenOfTarget(orbitList, item);
                }
            }

            return sum;
        }

        public int SolvePart2()
        {
            Dictionary<string, int> meParentDistances = GetParentDistances("YOU");
            Dictionary<string, int> santaParentDistances = GetParentDistances("SAN");

            var commonPoints = meParentDistances.Keys
                .Intersect(santaParentDistances.Keys);

            int minDistance = commonPoints.Min(cp =>
            {
                int meToCommonDistance = meParentDistances[cp];
                int santaToCommonDistance = santaParentDistances[cp];

                return meToCommonDistance + santaToCommonDistance;
            });

            return minDistance;
        }

        private Dictionary<string, int> GetParentDistances(string point)
        {
            Dictionary<string, int> parentDistances = new Dictionary<string, int>();

            string parentPoint = point;

            int i = 0;

            while (parentPoint != null)
            {
                parentPoint = GetParentOf(parentPoint);

                if (parentPoint != null)
                    parentDistances.Add(parentPoint, i);

                i++;
            }

            return parentDistances;
        }

        private string GetParentOf(string point)
        {
            var found = _rawOrbitData.Where(o => o.Item2 == point);

            if (!found.Any())
                return null;

            else return found.Single().Item1;
        }
    }
}