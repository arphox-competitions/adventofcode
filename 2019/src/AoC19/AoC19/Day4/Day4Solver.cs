﻿using System;
using System.IO;
using System.Linq;

namespace AoC19.Day4
{
    public sealed class Day4Solver
    {
        private readonly int _low;
        private readonly int _high;

        private const string InputPath = @"_inputs/day4input.txt";
        public static string ReadRealInput() => File.ReadAllLines(InputPath).Single();

        public Day4Solver(string line)
        {
            string[] parts = line.Split('-');
            _low = int.Parse(parts[0]);
            _high = int.Parse(parts[1]);
        }

        public int SolvePart1()
        {
            int counter = 0;
            for (int i = _low; i <= _high; i++)
            {
                if (Part1_MeetCriteria(i))
                {
                    counter++;
                }
            }

            return counter;
        }

        public int SolvePart2()
        {
            int counter = 0;
            for (int i = _low; i <= _high; i++)
            {
                if (Part2_MeetCriteria(i))
                {
                    counter++;
                }
            }

            return counter;
        }

        private bool Part1_MeetCriteria(int number)
        {
            string numberString = number.ToString();
            if (!(numberString[0] == numberString[1] ||
                numberString[1] == numberString[2] ||
                numberString[2] == numberString[3] ||
                numberString[3] == numberString[4] ||
                numberString[4] == numberString[5]))
            {
                return false;
            }

            for (int i = 1; i < numberString.Length; i++)
            {
                if (numberString[i - 1] > numberString[i])
                {
                    return false;
                }
            }

            return true;
        }

        public bool Part2_MeetCriteria(int number)
        {
            string str = number.ToString();

            // never decreases
            for (int i = 1; i < str.Length; i++)
                if (str[i - 1] > str[i])
                    return false;

            // two adjacents
            int a = GetCharIntegerValue(str[0]);
            int b = GetCharIntegerValue(str[1]);
            int c = GetCharIntegerValue(str[2]);
            int d = GetCharIntegerValue(str[3]);
            int e = GetCharIntegerValue(str[4]);
            int f = GetCharIntegerValue(str[5]);

            bool ab = a == b;
            bool bc = b == c;
            bool cd = c == d;
            bool de = d == e;
            bool ef = e == f;

            if (!(ab || bc || cd || de || ef))
                return false;

            if (ab && !bc ||
                bc && !ab && !cd ||
                cd && !bc && !de ||
                de && !cd && !ef ||
                ef && !de)
            {
                return true;
            }

            return false;
        }

        private static int GetCharIntegerValue(char ch) => (int)char.GetNumericValue(ch);
    }
}