﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AoC19.Day3
{
    public class Day3Solver
    {
        // X: left to right (0 .. inf)
        // Y: down to up (0 .. inf)

        private const string InputPath = @"_inputs/day3input.txt";

        private readonly string _wire1path;
        private readonly string _wire2path;

        public static string[] ReadRealInput() => File.ReadAllLines(InputPath);

        public Day3Solver(string[] fileLines)
        {
            _wire1path = fileLines[0];
            _wire2path = fileLines[1];
        }

        public int SolvePart1()
        {
            List<(int x, int y)> wire1points = Part1_ParseWirePath(_wire1path);
            List<(int x, int y)> wire2points = Part1_ParseWirePath(_wire2path);

            (int x, int y)[] intersectionPoints = wire1points.Intersect(wire2points).ToArray();
            int closest = intersectionPoints.Min(p => Math.Abs(p.x) + Math.Abs(p.y));
            return closest;
        }

        private List<(int x, int y)> Part1_ParseWirePath(string wirePath)
        {
            List<(int x, int y)> currentPath = new List<(int x, int y)>();
            string[] steps = wirePath.Split(',', StringSplitOptions.RemoveEmptyEntries);

            int currentX = 0;
            int currentY = 0;

            foreach (string step in steps)
            {
                char direction = step[0];
                int distance = int.Parse(step.Substring(1));

                for (int i = 0; i < distance; i++)
                {
                    switch (direction)
                    {
                        case 'U': currentY++; break;
                        case 'D': currentY--; break;
                        case 'L': currentX--; break;
                        case 'R': currentX++; break;
                    }

                    AddCurrentPath();
                }

            }

            return currentPath;

            // ----------
            void AddCurrentPath() => currentPath.Add((currentX, currentY));
        }

        public int SolvePart2()
        {
            Dictionary<(int x, int y), int> wire1 = Part2_ParseWirePath(_wire1path);
            Dictionary<(int x, int y), int> wire2 = Part2_ParseWirePath(_wire2path);

            (int x, int y)[] intersections = wire1.Keys.Intersect(wire2.Keys).ToArray();

            return intersections.Min(p => wire1[p] + wire2[p]);
        }

        private Dictionary<(int x, int y), int> Part2_ParseWirePath(string wirePath)
        {
            // value => distance
            Dictionary<(int x, int y), int> pointDistances = new Dictionary<(int x, int y), int>();
            string[] steps = wirePath.Split(',', StringSplitOptions.RemoveEmptyEntries);

            int currentX = 0;
            int currentY = 0;
            int stepCounter = 0;

            for (int i = 0; i < steps.Length; i++)
            {
                char direction = steps[i][0];
                int distance = int.Parse(steps[i].Substring(1));

                for (int j = 0; j < distance; j++)
                {
                    switch (direction)
                    {
                        case 'U': currentY++; break;
                        case 'D': currentY--; break;
                        case 'L': currentX--; break;
                        case 'R': currentX++; break;
                    }

                    pointDistances.TryAdd((currentX, currentY), ++stepCounter);
                }
            }

            return pointDistances;
        }
    }
}