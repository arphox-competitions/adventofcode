﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace AoC19.Day16
{
    public sealed class Day16Solver
    {
        private const string InputPath = @"_inputs/day16input.txt";
        public static string ReadRealInput() => File.ReadAllLines(InputPath).Single();
        private static readonly sbyte[] _basePattern = { 0, 1, 0, -1 };

        private sbyte[] _digits;
        private sbyte[][] _patternCache;

        public Day16Solver(string input)
        {
            _digits = input
                .Select(ch => (sbyte)char.GetNumericValue(ch))
                .ToArray();
        }

        public string SolvePart1()
        {
            _patternCache = GeneratePatterns(_digits.Length);

            int length = _digits.Length;
            sbyte[] newDigits = new sbyte[length];
            RunPhase(length, newDigits);

            return string.Concat(_digits.Take(8));
        }

        private void RunPhase(int length, sbyte[] newDigits)
        {
            for (int i = 0; i < 100; i++)
            {
                for (int j = 0; j < length; j++)
                {
                    sbyte[] pattern = _patternCache[j];
                    long result = 0;
                    for (int k = 0; k < length; k++)
                    {
                        result += pattern[k] * _digits[k];
                    }

                    newDigits[j] = (sbyte)Math.Abs(result % 10);
                }

                Array.Copy(newDigits, 0, _digits, 0, newDigits.Length);
            }
        }

        private sbyte[][] GeneratePatterns(int length)
        {
            sbyte[][] patterns = new sbyte[length][];

            for (int i = 0; i < length; i++)
            {
                patterns[i] = GetPatternEnumerator(i + 1).Take(length).ToArray();
            }

            return patterns;
        }

        internal IEnumerable<sbyte> GetPatternEnumerator(int position)
        {
            if (position <= 0) yield break;

            IEnumerator<sbyte> enumerator = Pattern().GetEnumerator();
            enumerator.MoveNext(); // skip first element
            while (enumerator.MoveNext())
                yield return enumerator.Current;

            IEnumerable<sbyte> Pattern()
            {
                while (true)
                {
                    for (int bp = 0; bp < _basePattern.Length; bp++)
                        for (int i = 0; i < position; i++)
                            yield return _basePattern[bp];
                }
            }
        }
    }
}