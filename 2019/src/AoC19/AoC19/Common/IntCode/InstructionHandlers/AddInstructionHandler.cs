﻿using System;

namespace AoC19.Common.IntCode.InstructionHandlers
{
    public sealed class AddInstructionHandler : InstructionHandler
    {
        public AddInstructionHandler(Func<long> relativeBaseOffsetProvider)
            : base(relativeBaseOffsetProvider)
        {
        }

        public override void Handle(long[] memory, ref long IP, ParameterMode[] paramModes)
        {
            long param1 = memory[IP++];
            long param2 = memory[IP++];
            long param3 = memory[IP++];

            long param1Value = GetParamValue(param1, paramModes[0], memory);
            long param2Value = GetParamValue(param2, paramModes[1], memory);
            long address = GetAddress(param3, paramModes[2]);

            memory[address] = param1Value + param2Value;
        }
    }
}