﻿using System;
using System.Diagnostics;

namespace AoC19.Common.IntCode.InstructionHandlers
{
    public abstract class InstructionHandler : IInstructionHandler
    {
        private readonly Func<long> _relativeBaseOffsetProvider;

        public InstructionHandler(Func<long> relativeBaseOffsetProvider)
        {
            _relativeBaseOffsetProvider = relativeBaseOffsetProvider;
        }

        public abstract void Handle(long[] memory, ref long IP, ParameterMode[] paramModes);

        protected long GetParamValue(long param, ParameterMode parameterMode, long[] memory)
        {
            switch (parameterMode)
            {
                case ParameterMode.Immediate:
                    return param;
                case ParameterMode.Position:
                    return memory[param];
                case ParameterMode.Relative:
                    return memory[_relativeBaseOffsetProvider() + param];
                default:
                    Debugger.Break();
                    throw new Exception("Unexpected parameter mode!");
            }
        }

        protected long GetAddress(long param, ParameterMode parameterMode)
        {
            switch (parameterMode)
            {
                case ParameterMode.Position:
                    return param;
                case ParameterMode.Relative:
                    long relativeBaseOffset = _relativeBaseOffsetProvider();
                    long address = relativeBaseOffset + param;
                    return address;
                default:
                    Debugger.Break();
                    throw new Exception("Error?");
            }
        }
    }
}