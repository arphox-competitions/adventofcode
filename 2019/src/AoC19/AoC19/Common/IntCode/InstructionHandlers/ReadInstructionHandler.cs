﻿using System;
using System.Collections.Generic;

namespace AoC19.Common.IntCode.InstructionHandlers
{
    public sealed class ReadInstructionHandler : InstructionHandler
    {
        private readonly Queue<long> _input;

        public ReadInstructionHandler(Queue<long> input, Func<long> relativeBaseOffsetProvider)
            : base(relativeBaseOffsetProvider)
        {
            _input = input;
        }

        public override void Handle(long[] memory, ref long IP, ParameterMode[] paramModes)
        {
            long param = memory[IP++];
            long address = GetAddress(param, paramModes[0]);
            memory[address] = _input.Dequeue();
        }
    }
}