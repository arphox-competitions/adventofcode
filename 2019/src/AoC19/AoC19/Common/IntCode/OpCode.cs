﻿namespace AoC19.Common.IntCode
{
    public enum OpCode
    {
        Add = 1,
        Multiply = 2,
        Read = 3,
        Print = 4,
        JumpIfTrue = 5,
        JumpIfFalse = 6,
        LessThan = 7,
        Equals = 8,
        IncRelativeBase = 9,
        Halt = 99
    }
}