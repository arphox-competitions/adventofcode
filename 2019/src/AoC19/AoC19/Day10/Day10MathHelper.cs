﻿using System;

namespace AoC19.Day10
{
    public static class Day10MathHelper
    {
        public static double GetAngle(int dirX, int dirY)
        {
            dirY = -dirY; // Up is down, down is up

            double atan = Math.Atan2(dirY, dirX);
            double angle = (((360 - ((RadianToDegree(atan) + 360) % 360)) % 360) + 90) % 360;

            return angle;
        }

        private static double RadianToDegree(double angle) => angle * (180.0 / Math.PI);
        //public static double DegreeToRadian(double angle) => Math.PI * angle / 180.0;
    }
}