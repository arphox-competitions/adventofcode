﻿namespace AoC19.Day10
{
    public sealed class PointWithAngle
    {
        public int X { get; }
        public int Y { get; }
        public double Angle { get; }

        public PointWithAngle(int x, int y, double angle)
        {
            X = x;
            Y = y;
            Angle = angle;
        }

        public override string ToString() => $"({X}, {Y}) A:{Angle:F2}";
    }
}