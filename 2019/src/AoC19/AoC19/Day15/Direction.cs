﻿using System;

namespace AoC19.Day15
{
    public enum Direction
    {
        Up = 1,
        Down = 2,
        Left = 3,
        Right = 4
    }

    public static class DirectionExtensions
    {
        public static void ThrowIfNotValidEnumMember(this Direction command)
        {
            if (!Enum.IsDefined(typeof(Direction), command))
                throw new Exception("Invalid movement");
        }
       
        public static (int x, int y) GetNeighborPositionTowards(this (int x, int y) pos, Direction direction)
        {
            return direction switch
            {
                Direction.Right => (pos.x + 1, pos.y),
                Direction.Left => (pos.x - 1, pos.y),
                Direction.Up => (pos.x, pos.y - 1),
                Direction.Down => (pos.x, pos.y + 1),
            };
        }

        public static Direction TurnRight(this Direction command)
        {
            return command switch
            {
                Direction.Right => Direction.Down,
                Direction.Down => Direction.Left,
                Direction.Left => Direction.Up,
                Direction.Up => Direction.Right
            };
        }

        public static Direction TurnLeft(this Direction command)
        {
            return command switch
            {
                Direction.Up => Direction.Left,
                Direction.Left => Direction.Down,
                Direction.Down => Direction.Right,
                Direction.Right => Direction.Up,
            };
        }

        public static Direction GetOpposite(this Direction command)
        {
            return command switch
            {
                Direction.Up => Direction.Down,
                Direction.Down => Direction.Up,
                Direction.Right => Direction.Left,
                Direction.Left => Direction.Right
            };
        }
    }
}