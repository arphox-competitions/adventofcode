﻿using AoC19.Common.IntCode;
using System;

namespace AoC19.Day15
{
    public sealed class Day15Game
    {
        private const int DIRECTION_NORTH = 1;
        private const int DIRECTION_SOUTH = 2;
        private const int DIRECTION_WEST = 3;
        private const int DIRECTION_EAST = 4;
        private const int STATUS_HITWALL = 0;
        private const int STATUS_MOVED = 1;
        private const int STATUS_MOVED_FOUND = 2;
        private readonly Random RandomGenerator = new Random(906_295); // speed: 120_367

        private readonly IntCodeVM _vm;
        private readonly bool _shouldPrint;
        private readonly bool _fastRandomDiscovery;
        private (int x, int y) _currentPosition;

        public Day15Game(string input, bool shouldPrint = true, bool fastRandomDiscovery = false)
        {
            _vm = new IntCodeVM(input);
            _shouldPrint = shouldPrint;
            _fastRandomDiscovery = fastRandomDiscovery;
        }

        public int Run()
        {
            InitializeRun();
            while (true)
                PerformStep();
        }

        private void InitializeRun()
        {
            Console.CursorVisible = false;
            const int width = 42;
            const int height = 42;
            Console.SetWindowSize(width, height);
            _currentPosition = (width / 2, height / 2);
            Print(_currentPosition.x, _currentPosition.y, 'D');
        }

        private void PerformStep()
        {
            int movement = GetMovement();
            int statusCode = MoveDroid(movement);
            (int x, int y) newPosition = ApplyMovementToCurrentPosition(movement);

            if (statusCode != STATUS_HITWALL)
            {
                Print(_currentPosition.x, _currentPosition.y, '.');
                _currentPosition = newPosition;

                char ch = statusCode == STATUS_MOVED ? 'D' : 'X';
                Print(_currentPosition.x, _currentPosition.y, ch);
            }
            else
                Print(newPosition.x, newPosition.y, '#');
        }

        private int MoveDroid(int movement)
        {
            _vm.Input.Enqueue(movement);
            _vm.Run();
            int statusCode = (int)_vm.Output.Dequeue();
            if (_vm.Output.Count > 0) throw new Exception("Single output expected");
            return statusCode;
        }

        private (int x, int y) ApplyMovementToCurrentPosition(int movement)
        {
            return movement switch
            {
                DIRECTION_EAST => (_currentPosition.x + 1, _currentPosition.y),
                DIRECTION_WEST => (_currentPosition.x - 1, _currentPosition.y),
                DIRECTION_NORTH => (_currentPosition.x, _currentPosition.y - 1),
                DIRECTION_SOUTH => (_currentPosition.x, _currentPosition.y + 1),
            };
        }

        private int GetMovement()
        {
            if (_fastRandomDiscovery)
                return RandomGenerator.Next(DIRECTION_NORTH, DIRECTION_EAST + 1);
            else
                return GetMovementFromConsole();
        }

        private static int GetMovementFromConsole()
        {
            int movement = -1;
            while (movement == -1)
                movement = AskAgain();

            return movement;

            static int AskAgain()
            {
                ConsoleKeyInfo consoleKeyInfo = Console.ReadKey(true);
                return consoleKeyInfo.Key switch
                {
                    ConsoleKey.W => DIRECTION_NORTH,
                    ConsoleKey.S => DIRECTION_SOUTH,
                    ConsoleKey.A => DIRECTION_WEST,
                    ConsoleKey.D => DIRECTION_EAST,
                    _ => -1,
                };
            }
        }

        private void Print(int x, int y, char c)
        {
            if (!_shouldPrint)
                return;

            Console.SetCursorPosition(x, y);
            Console.Write(c);
        }
    }
}