﻿namespace AoC19.Day15
{
    public enum StatusCode
    {
        HitWallDidNotMove = 0,
        Moved = 1,
        MovedAndFoundTarget = 2
    }
}