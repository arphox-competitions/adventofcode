﻿namespace AoC19.Day11
{
    public enum PaintColor
    {
        Black = 0,
        White = 1
    }
}